package P7Question2;

public class TestBookList {
	public static void main(String argv[]) {
		BookLinkedList list = new BookLinkedList();
		ListNode p;
		
		Book john = new Book("1234", "John");
		Book eric = new Book("5678", "Eric");
		Book phoon = new Book("9999", "Phoon");
		
		System.out.println("");
		System.out.println("====================");
		System.out.println("Inserting books");
		System.out.println("====================");
		
		list.insertFirst(john);
		list.insertFirst(eric);
		list.displayList(); //Print the contents
		
		
		System.out.println("");
		System.out.println("====================");
		System.out.println("Finding Phoon");
		System.out.println("====================");
		p = list.find(phoon);
		
		if (p != null) {
			System.out.println("Got em!");
		}
		else {
			System.out.println("Don't have leh");
		}
		
		System.out.println("");
		System.out.println("====================");
		System.out.println("Inserting Phoon book");
		System.out.println("====================");
		
		list.insertAfter(eric, phoon);
		list.displayList();
		
		System.out.println("");
		System.out.println("====================");
		System.out.println("Invoke find method");
		System.out.println("====================");
		
		Book findjohn = new Book("5678", "John");
		p = list.find(findjohn);
		
		if (p != null) {
			System.out.println("Ha! Got em!");
		}
		else {
			System.out.println("Don't have leh");
		}
	}
}
