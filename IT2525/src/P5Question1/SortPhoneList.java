package P5Question1;

import java.util.*;


public class SortPhoneList{
	//-----------------------------------------------------------------
	//  Creates an array of Contact objects, sorts them, then prints
	//  them.
	//-----------------------------------------------------------------
	public static void main (String[] args){
		ArrayList<Contact> contactlist = new ArrayList<Contact>();
		
		Contact new1 = new Contact("Angie", "Chu", "999");
		Contact new2 = new Contact("Ryan", "Chew", "888");
		Contact new3 = new Contact("Joanne", "Lim", "777");
		Contact new4 = new Contact("Seri", "NA", "666");
		Contact new5 = new Contact("Aye", "NA", "555");
		Contact new6 = new Contact("Peter", "See", "444");
		Contact new7 = new Contact("Daryl", "Huang", "333");
		Contact new8 = new Contact("Eugene", "Hoong", "222");
		Contact new9 = new Contact("Eric", "Koh", "111");
		Contact new10 = new Contact("Wan Ching", "NA", "000");
		
		contactlist.add(new1);
		contactlist.add(new2);
		contactlist.add(new3);
		contactlist.add(new4);
		contactlist.add(new5);
		contactlist.add(new6);
		contactlist.add(new7);
		contactlist.add(new8);
		contactlist.add(new9);
		contactlist.add(new10);
		
		
		Collections.sort(contactlist);
		
		for (int i = 0 ; i < contactlist.size(); i++) {
			System.out.println(contactlist.get(i));
		}
	}
}
