package P7Question1;

// Sample code for creating and printing the linked list.
public class TestLinkedList{
	private static ListNode pHead;
	
	public static ListNode deleteFirst() {
		ListNode p = null;
		// check if list is not empty
		if (!isEmpty()) {
			p = pHead;
			pHead = pHead.next;
			p.next = null;
		}
		return p;
	}
	
	public static boolean isEmpty() {
		return (pHead == null);
	}
	
	public static void displayList() {
		System.out.print("Linked List: ");
		ListNode current = pHead; // start of list

		// loop through until end of list

		while (current != null) {
			System.out.print(current.data + " ");
			current = current.next; // move to next link
		}
		System.out.println();
	}
	
	public static ListNode find(int key) {
		ListNode p = pHead;
		// if list is empty
		if (p == null)
			return null;

		while (p.data != key) {
			if (p.next == null) // if end of list
				return null; // not found
			else
				p = p.next;
		}
		return p; // find it
	}
	
	public static void insertFirst(int value) {
		ListNode newNode = new ListNode(value);
		newNode.next = pHead;
		pHead = newNode;
	}
	
	public static ListNode insertAfter(int key, int value) {
		ListNode p = find(key);
		// if list is empty or key not found,
		// insert fail
		if (p == null)
			return null;
		ListNode newNode = new ListNode(value);
		newNode.next = p.next;
		p.next = newNode;
		return newNode;
	}
	
	public static ListNode delete(int key) {
		ListNode curr = pHead;
		ListNode prev = pHead;

		if (curr == null) // if list is empty
			return null;

		while (curr.data != key) {
			if (curr.next == null) // if end of list
				return null; // not found
			else {
				prev = curr;
				curr = curr.next;
			}
		}
		// find it
		if (curr == pHead) // if the first node
			pHead = pHead.next;
		else
			prev.next = curr.next;
		// set the next ref to null
		curr.next = null;
		return curr;
	}
	
	public static void main (String argv[]){
		for (int i = 1 ; i <= 10 ; i++) {
			ListNode newNode = new ListNode(i);
			newNode.next = pHead;
			pHead = newNode;
		}
		
		displayList();
		
		System.out.println("Deleted node: " + deleteFirst());
		
		System.out.println("Found node: " + find(5));
		
		System.out.println("No such node: " + find(20));
		
		System.out.println("Before inserting...");
		
		displayList();
		
		System.out.println("Node inserted: 20");
		insertAfter(5, 20);
		
		System.out.println("After inserting...");
		
		displayList();
		
		System.out.println("Before deleting...");
		
		displayList();
		
		System.out.println("Node deleted: 5");
		delete(5);
		
		System.out.println("After deleting...");
		
		displayList();
	}
}
