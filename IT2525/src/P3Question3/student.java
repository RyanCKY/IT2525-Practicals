package P3Question3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class student {
	
	private String adminNo;
	private String name;
	private GregorianCalendar birthDate = new GregorianCalendar();
	private int test1, test2, test3;
	private String studentRecord;
	
	public static GregorianCalendar convertdate (String date) {
		
		int day = Integer.parseInt(date.substring(0, 1));
		int month = Integer.parseInt(date.substring(3, 4));
		int year = Integer.parseInt(date.substring(6, 9));
		
		GregorianCalendar current  = new GregorianCalendar(year, month, day);
		
		return current;	
	}
	
	public student (String adminNo, String name, String birthDate, int test1, int test2, int test3) {
		
		
		this.adminNo = adminNo;
		this.name = name;
		this.birthDate = convertdate(birthDate);
		this.test1 = test1;
		this.test2 = test2;
		this.test3 = test3;
	}
	
	public student (String studentRecord) {
		Scanner sc = new Scanner(System.in);
		sc.useDelimiter(";");
		this.adminNo = adminNo;
		this.name = name;
		this.test1 = test1;
		this.test2 = test2;
		this.test3 = test3;
	}

	public String getAdminNo() {
		return adminNo;
	}

	public void setAdminNo(String adminNo) {
		this.adminNo = adminNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GregorianCalendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(GregorianCalendar birthDate) {
		this.birthDate = birthDate;
	}

	public int getTest1() {
		return test1;
	}

	public void setTest1(int test1) {
		this.test1 = test1;
	}

	public int getTest2() {
		return test2;
	}

	public void setTest2(int test2) {
		this.test2 = test2;
	}

	public int getTest3() {
		return test3;
	}

	public void setTest3(int test3) {
		this.test3 = test3;
	}
	
	public double returnAverage (int total) {
		double average = total / 3;
		return average;
	}
	
	public String toString () {
		String concatenation = adminNo + name + birthDate + Integer.toString(test1) + Integer.toString(test2) + Integer.toString(test3);
		return concatenation;
	}
	
	public static void main (String[] args) {
		try {
			FileReader fr = new FileReader("student.txt");
			Scanner sc = new Scanner(fr);
			
			student st = new student(sc.nextLine());
			System.out.println(st);
			
			fr.close();
		} catch (FileNotFoundException exception) {
			System.out.println("The file is not found");
		} catch (IOException exception) {
			System.out.println(exception);
		}
	}
	
	public static ArrayList<student> readStudent (String file){
        // declare and instantiate a FileController object using input parameter file
        FileController input = new FileController(file);
		
		
        ArrayList<student> recs = new ArrayList<student>();
        ArrayList<String> recsReturn;

        // invoke the readLine methods in FileController to read
        // records from file and stores in variable recsReturn
        recsReturn = input.readLine();
        
        // write a for loop to create student objects and
        // store them in arraylist recs
        for (int i = 0 ; i < 1 ; i++) {
        	student s1 = new student (recsReturn.get(i));
        }
        
        return recs;
   }
}
