package P3Question3;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class UI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UI frame = new UI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UI() {
		setTitle("Update Student Details");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAdminNo = new JLabel("Admin No");
		lblAdminNo.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdminNo.setBounds(10, 39, 80, 26);
		contentPane.add(lblAdminNo);
		
		JLabel lblName = new JLabel("Name");
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setBounds(10, 76, 80, 26);
		contentPane.add(lblName);
		
		JLabel lblBirthdate = new JLabel("Birthdate");
		lblBirthdate.setHorizontalAlignment(SwingConstants.CENTER);
		lblBirthdate.setBounds(10, 113, 80, 26);
		contentPane.add(lblBirthdate);
		
		JLabel lblTest = new JLabel("Test 1");
		lblTest.setHorizontalAlignment(SwingConstants.CENTER);
		lblTest.setBounds(10, 150, 80, 26);
		contentPane.add(lblTest);
		
		JLabel lblTest_1 = new JLabel("Test 2");
		lblTest_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblTest_1.setBounds(10, 187, 80, 26);
		contentPane.add(lblTest_1);
		
		JLabel lblTest_2 = new JLabel("Test 3");
		lblTest_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblTest_2.setBounds(10, 224, 80, 26);
		contentPane.add(lblTest_2);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(97, 39, 314, 26);
		contentPane.add(comboBox);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(96, 79, 315, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(97, 113, 314, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		textField_2.setBounds(96, 153, 315, 23);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		textField_3.setBounds(91, 190, 320, 23);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setEditable(false);
		textField_4.setBounds(90, 227, 321, 23);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
	}
}
