package P8Question1;

import java.util.Scanner;
import java.util.Stack;

public class Question1 {
	public static void main(String [] args) {
		Stack<String> stack = new Stack<String>();
		Scanner in = new Scanner(System.in);
		
		String input = "";
		
		if(stack.empty()) {
			System.out.println("An empty stack is created.\n");
		}
		
		//Put in stack
		while (!input.equalsIgnoreCase("quit")) {
			System.out.print("Enter a word: ");
			input = in.nextLine();
			
			if (!input.equals("quit")) {
				stack.push(input);
			}
		}
		
		System.out.println("");
		
		//Search for a value
		System.out.print("Enter a word to search: ");
		String search = in.nextLine();
		int position = stack.search(search);
		
		System.out.println(search + " is at position " + position + " from the top");
		
		System.out.println("");
		
		//Check value at top
		String top = stack.peek();
		System.out.println(top + " is at the top of the stack.");
		
		System.out.println("");
		
		System.out.print("Words in reverse order: ");
		//Words in reverse order
		while (!stack.empty()) {
			System.out.print(stack.pop() + " ");
		}
	}
}
