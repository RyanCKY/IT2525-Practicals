package P3Question2;

import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FileController {
	private static String FileName;
	
	public FileController (String FileName) {
		this.FileName = FileName;
	}
	
	public static ArrayList<String> readLine() {
		ArrayList<String> records = new ArrayList<String>();
		String file = "student.txt";
		String line = null;
		String path = Paths.get("student.txt").toString();
		
		try{
		       FileReader fr = new FileReader (file);
		       Scanner sc = new Scanner(fr);

				 //read in the file line by line
		       line = Files.readAllLines(Paths.get("student.txt")).get(0);
		       
		       records.add(line);

		       fr.close();
		    }
		    catch (FileNotFoundException exception){
		       System.out.println ("The file " + file + " was not found.");
		    }
		    catch (IOException exception){
		       System.out.println (exception);
		    }
		
		return records;
	}
	
	public static void writeLine (ArrayList<String> input) throws IOException {
		String file = "student.txt";
		
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter outFile = new PrintWriter(bw);
		
		outFile.println(input);
		
		outFile.close();
	}
	
	public static void writeLine (String input) throws IOException {
		String file = "student.txt";
		
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		PrintWriter outFile = new PrintWriter(bw);
		
		outFile.println(input);
		
		outFile.close();
	}
}
