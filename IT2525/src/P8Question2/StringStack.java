package P8Question2;

import java.util.Scanner;
import java.util.Stack;

public class StringStack {
	public static void main (String [] args) {
		Stack<Object> stack = new Stack<Object>();
		Scanner in = new Scanner(System.in);
		
		System.out.print("Enter a word: ");
		String input = in.nextLine();
		
		for (int i = 0 ; i < input.length(); i++) {
			char character = input.charAt(i);
			stack.push(character);
		}
		
		System.out.println("");
		
		System.out.print("The word reversed is: ");
		while(!stack.empty()) {
			System.out.print(stack.pop() + " ");
		}
	}
}
