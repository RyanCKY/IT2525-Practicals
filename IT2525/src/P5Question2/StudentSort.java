package P5Question2;

import java.util.ArrayList;
import java.util.Collections;


//==============================================================
//Program Name:	StudentSort.java (Practical 5 Q 2)
// Description:	...
//==============================================================
//import java.util.*;

public class StudentSort {
	public static void main (String args[]){
		ArrayList<Student> studentlist = new ArrayList<Student>();
		
		Student new1 = new Student("155126N", "Ryan");
		Student new2 = new Student("155128N", "James");
		Student new3 = new Student("155125N", "John");
		Student new4 = new Student("145124N", "Mark");
		Student new5 = new Student("135798N", "Tom");
		
		studentlist.add(new1);
		studentlist.add(new2);
		studentlist.add(new3);
		studentlist.add(new4);
		studentlist.add(new5);
		
		Collections.sort(studentlist);
		
		for (int i = 0 ; i < studentlist.size() ; i++) {
			System.out.println(studentlist.get(i));
		}
	}
}