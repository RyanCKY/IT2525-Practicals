package P5Question4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import P5Question4.FileController;
import P5Question4.Student;

public class StudentApp {
	public static void main(String [] args) throws IOException {
		// declare and instantiate a FileController object
	     // for text file "student.txt"
		FileController file = new FileController("student.txt");
	     
	     ArrayList<String> recs = new ArrayList<String>();
	     ArrayList<String> recsReturn;
	 
	     recs.add("031111A;Mary Tan;1/06/1981;100;100;90");
	     recs.add("031112B;John Tan;1/07/1981:90;100;75");
	     recs.add("031113C;Harry Lim;1/08/1982;75;80;75");
	     // add 2 more records to the recs ArrayList
	     
	 
	     // invoke the writeLine method in FileController
	     // to write records to file
	     FileController.writeLine(recs);
	     
	    
	     // invoke the readLine methods in FileController to read
	     // records from file and stores the records in variable recsReturn
	     recsReturn = FileController.readLine();
	     
	    
	     // write a for loop to print out names of the students
	     for (int i = 0 ; i < recsReturn.size(); i++) {
	    	 System.out.println(recsReturn.get(i));
	     }
	     
	     Collections.sort(recsReturn);
	     
	     System.out.println("After Sorting");
	     
	     for (int i = 0 ; i < recsReturn.size(); i++) {
	    	 System.out.println(recsReturn.get(i));
	     }
	     
	     //Student.readStudent("student.txt");
	}
}
