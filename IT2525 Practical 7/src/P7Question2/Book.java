package P7Question2;

public class Book {
	private String isbn;
	private String author;
	
	//Constructor
	public Book (String i, String a) {
		isbn = i;
		author = a;
	}
	
	//Override toString() in Object class
	public String toString() {
		return isbn + " " + author;
	}
	
	//Override equals() in Object class
	public boolean equals(Object obj) {
		Book b = (Book) obj;
		return (this.isbn.equals(b.isbn));
	}
}
