//================================================================
// Program Name:	Student.java (Practical 5 Q 2)
// Description:		Student class implements Comparable Interface
//================================================================
package P5Question2;

import java.util.*;

public class Student implements Comparable<Student>{

	// Declare Instance variables
	private String adminNo;
	private String name;

	public Student(String adminNo, String name) {
		this.adminNo = adminNo;
		this.name = name;
	}

	// ----------------------------------------------------------
	// Accessor methods to get and set attributes
	// ----------------------------------------------------------
	public String getAdminNo() {
		return adminNo;
	}

	public String getName() {
		return name;
	}

	public void setAdminNo(String adminNo) {
		this.adminNo = adminNo;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int compareTo(Student c) {
		int result;
		
		result = name.compareTo(c.name);
		
		return result;
	}

	@Override
	public String toString() {
		return "Student [adminNo=" + adminNo + ", name=" + name + "]";
	}

	
	
}
