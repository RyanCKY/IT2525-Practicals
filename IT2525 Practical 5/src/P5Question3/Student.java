//================================================================
// Program Name:	Student.java (Practical 5 Q 2)
// Description:		Student class implements Comparable Interface
//================================================================
package P5Question3;

import java.util.*;

public class Student implements Comparable<Student>{

	// Declare Instance variables
	private String adminNo;
	private String name;
	private int age;

	public Student(String adminNo, String name, int age) {
		this.adminNo = adminNo;
		this.name = name;
		this.age = age;
	}

	// ----------------------------------------------------------
	// Accessor methods to get and set attributes
	// ----------------------------------------------------------
	public String getAdminNo() {
		return adminNo;
	}

	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}

	public void setAdminNo(String adminNo) {
		this.adminNo = adminNo;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		this.age = age;
	}

	public int compareTo(Student c) {
		int result = 0;
		
		result = name.compareTo(c.name);
		
		if (name.equals(c.name)) {
			if (age < c.age) {
				result = 1;
			}
			else if (age > c.age){
				result = -1;
			}
			else {
				result = 0;
			}
		}
		
		return result;
	}

	@Override
	public String toString() {
		return "Student [adminNo=" + adminNo + ", name=" + name + ", age=" + age + "]";
	}

	

	
	
}
