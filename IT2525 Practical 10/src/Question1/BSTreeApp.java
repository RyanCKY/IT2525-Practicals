package Question1;

import java.util.*;
import Question1.BSTree;

public class BSTreeApp {
	public static void main(String[] args) {
		BSTree tree = new BSTree();
		
		System.out.println("===============================================");
		System.out.println("Inserting values: 28, 14, 21, 35, 7, 84, 56, 42");
		System.out.println("===============================================");
		
		tree.add(28);
		tree.add(14);
		tree.add(21);
		tree.add(35);
		tree.add(7);
		tree.add(84);
		tree.add(56);
		tree.add(42);
		
		System.out.println("");
		System.out.println("===============================================");
		System.out.println("Displaying Tree in order...");
		tree.getRoot().inOrderPrint();
		System.out.println("");
		System.out.println("===============================================");
		
		System.out.println("");
		System.out.println("===============================================");
		System.out.println("Displaying Tree pre order...");
		tree.getRoot().preOrderPrint();
		System.out.println("");
		System.out.println("===============================================");
		
		System.out.println("");
		System.out.println("===============================================");
		System.out.println("Displaying Tree post order...");
		tree.getRoot().postOrderPrint();
		System.out.println("");
		System.out.println("===============================================");
	}
}
