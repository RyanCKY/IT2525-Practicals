package Question1;

import java.util.Scanner;

public class QueueApp {
	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		ListQueue lq = new ListQueue();
		
		int choice;
		
		System.out.println("====================");
		System.out.println("Queue Operation Menu");
		System.out.println("====================");
		System.out.println("");
		System.out.println("1.    Enqueue       ");
		System.out.println("2.    Dequeue       ");
		System.out.println("3.  Check if Empty  ");
		System.out.println("4.  Count Total     ");
		System.out.println("5.   View Queue     ");
		System.out.println("");
		
		do {
			System.out.print("Enter decision: ");
			choice = in.nextInt();
			
			in.nextLine();
			
			if (choice == 1) {
				System.out.print("Enter string to queue: ");
				String input = in.nextLine();
				
				lq.enqueue(input);
			}
			else if (choice == 2) {
				String result = (String) lq.dequeue();
				System.out.println(result + " has been removed from the queue.");
			}
			else if (choice == 3) {
				boolean result = lq.isEmpty();
				
				if (result == true) {
					System.out.println("The queue is empty");
				}
				else if (result == false) {
					System.out.println("The queue is not empty");
				}
				else {
					System.out.println("Error 404");
				}
			}
			else if (choice == 4) {
				int result = lq.size();
				System.out.println("Total number of names in the queue: " + result);
			}
			else if (choice == 5) {
				lq.viewQueue();
			}
			else {
				System.out.println("ERROR INPUT. DECIDE PROPERLY DUMBASS.");
			}
			
		} while (choice != 0);
		System.out.println("Copyrighted by RobCo Industries 2075-2077");
	}
}
