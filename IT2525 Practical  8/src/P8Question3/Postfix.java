package P8Question3;

import java.util.Stack;

public class Postfix {
	public static void main (String [] args) {
		Stack<Object> stack = new Stack<Object>();
		
		String input = "((15 - 5) / 2) + (30 / (3 * 5))";
		
		//get first character of the expression
		for (int i = 0 ; i < input.length(); i++) {
			char character = input.charAt(i);
			
			while (i != input.length()) {
				if (character == '(') {
					stack.push(character);
				}
				else if (character == ')') {
					if (stack.isEmpty() == true) {
						System.out.println("ERROR");
						System.exit(0);
					}
					else {
						System.out.println(stack.pop());
					}
				}
			}
		}
		
		if (stack.isEmpty() == false) {
			System.out.println("ERROR");
			System.exit(0);
		}
	}
}
