package P6Question3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import P6Question3.student;


public class studentsearch {
	public static void main (String[] args) {
		Scanner in = new Scanner(System.in);
	
		ArrayList<student> studentList = student.readStudent("student.txt");
		ArrayList<String> adminList = new ArrayList<String>();
		
		for (int i = 0 ; i < studentList.size(); i++) {
			adminList.add(studentList.get(i).getAdminNo());
		}
		
		//Collections.sort(adminList);
		System.out.println(studentList.size());
		System.out.print("Enter admin number: ");
		String input = in.nextLine();
		
		while (!input.equals("quit")) {
			Collections.sort(adminList);
			int pos = Collections.binarySearch(adminList, input);
			
			if (pos < 0) {
				System.out.println("Student not found. Are you dumb or something?");
			}
			else if (pos >= 0) {
				System.out.println("Name: " + studentList.get(pos).getName());
				System.out.println("Test 1: " + studentList.get(pos).getTest1());
				System.out.println("Test 2: " + studentList.get(pos).getTest2());
				System.out.println("Test 3: " + studentList.get(pos).getTest3());
			}
			
			System.out.println("Enter admin number: ");
			input = in.nextLine();
		}
		
		if (input.equals("quit")) {
			System.out.println("This dev hates you. Bye.");
			System.exit(0);
		}
	}
}
